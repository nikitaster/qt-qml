import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Page {
    id: page

    header: RowLayout {
        id: navbar

        Label {
            id: title
            text: qsTr("ЛР 1. Основы Qt QML")
            font.pixelSize: Qt.application.font.pixelSize * 1.5
            Layout.fillWidth: true
            Layout.fillHeight: true
            padding: 10
            color: "#8910af"
        }

        Item {
            id: item1
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.preferredWidth: 36

            Button {
                width: 32
                height: 32
                anchors.verticalCenter: parent.verticalCenter
                background: null
            }

            Image {
                id: image_bell
                width: 32
                height: 32
                anchors.verticalCenter: parent.verticalCenter
                source: "images/notification.png" /* for macos use .svg/.icns, for android/win - png*/
            }
        }

        Item {
            Layout.fillHeight: true
            Layout.preferredWidth: 36
            Button {
                width: 32
                height: 32
                anchors.verticalCenter: parent.verticalCenter
                background: null
            }

            Image {
                id: image_settings
                source: "images/settings.png" /* for macos use .svg/.icns, for android/win - png*/
                width: 32
                height: 32
                anchors.verticalCenter: parent.verticalCenter
            }
        }
    }

    GridLayout {
        anchors.fill: parent
        anchors.margins: 10
        columns: 4
        columnSpacing: 12
        rowSpacing: 24



        Switch {
            id: switch1
            text: qsTr("Switch")

            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            Layout.row: 0
            Layout.rowSpan: 1
            Layout.column: 2
            Layout.columnSpan: 2

            indicator: Rectangle {
                    implicitWidth: 48
                    implicitHeight: 26
                    x: switch1.leftPadding
                    y: parent.height / 2 - height / 2
                    radius: 13
                    color: switch1.checked ? "#17a81a" : "#ffffff"
                    border.color: switch1.checked ? "#17a81a" : "#cccccc"

                    Rectangle {
                        x: switch1.checked ? parent.width - width : 0
                        width: 26
                        height: 26
                        radius: 13
                        color: switch1.down ? "#cccccc" : "#ffffff"
                        border.color: switch1.checked ? (switch1.down ? "#17a81a" : "#21be2b") : "#999999"
                    }
                }

                contentItem: Text {
                    text: switch1.text
                    font: switch1.font
                    opacity: enabled ? 1.0 : 0.3
                    color: switch1.down ? "#17a81a" : "#21be2b"
                    verticalAlignment: Text.AlignVCenter
                    leftPadding: switch1.indicator.width + switch1.spacing
                }
        }

        Text {
            id: text1
            text: qsTr("Green")
            font.pixelSize: Qt.application.font.pixelSize * 2

            Layout.fillWidth: false
            Layout.row: 0
            Layout.rowSpan: 1
            Layout.column: 0
            Layout.columnSpan: 2

            color: "#21be2b"

        }

        BusyIndicator {
            id: busyIndicator
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            Layout.row: 0
            Layout.rowSpan: 1
            Layout.column: 0
            Layout.columnSpan: 4

            contentItem: Item {
                    implicitWidth: 64
                    implicitHeight: 64

                    Item {
                        id: item123
                        x: parent.width / 2 - 32
                        y: parent.height / 2 - 32
                        width: 64
                        height: 64
                        opacity: busyIndicator.running ? 1 : 0

                        RotationAnimator {
                            target: item123
                            running: busyIndicator.visible && busyIndicator.running
                            from: 0
                            to: 360
                            loops: Animation.Infinite
                            duration: 1250
                        }

                        Repeater {
                            id: repeater
                            model: 8

                            Rectangle {
                                x: item123.width / 2 - width / 2
                                y: item123.height / 2 - height / 2
                                implicitWidth: 10
                                implicitHeight: 10
                                radius: 5
                                color: "#21be2b"
                                transform: [
                                    Translate {
                                        y: -Math.min(item123.width, item123.height) * 0.5 + 5
                                    },
                                    Rotation {
                                        angle: index / repeater.count * 360
                                        origin.x: 5
                                        origin.y: 5
                                    }
                                ]
                            }
                        }
                    }
                }
        }

        Slider {
            id: slider
            value: 0.5

            Layout.row: 1
            Layout.rowSpan: 2
            Layout.column: 0
            Layout.columnSpan: 2
            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
            Layout.fillWidth: true

            background: Rectangle {
                    x: slider.leftPadding
                    y: slider.topPadding + slider.availableHeight / 2 - height / 2
                    implicitWidth: 200
                    implicitHeight: 4
                    width: slider.availableWidth
                    height: implicitHeight
                    radius: 2
                    color: "#bdbebf"

                    Rectangle {
                        width: slider.visualPosition * parent.width
                        height: parent.height
                        color: "#21be2b"
                        radius: 2
                    }
                }

                handle: Rectangle {
                    x: slider.leftPadding + slider.visualPosition * (slider.availableWidth - width)
                    y: slider.topPadding + slider.availableHeight / 2 - height / 2
                    implicitWidth: 26
                    implicitHeight: 26
                    radius: 13
                    color: slider.pressed ? "#f0f0f0" : "#f6f6f6"
                    border.color: "#bdbebf"
                }
        }

        Dial {
            id: dial

            Layout.row: 1
            Layout.rowSpan: 2
            Layout.column: 2
            Layout.columnSpan: 2
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter

            background: Rectangle {
                    x: dial.width / 2 - width / 2
                    y: dial.height / 2 - height / 2
                    width: Math.max(64, Math.min(dial.width, dial.height))
                    height: width
                    color: "transparent"
                    radius: width / 2
                    border.color: dial.pressed ? "#17a81a" : "#21be2b"
                    opacity: dial.enabled ? 1 : 0.3
                }

            handle: Rectangle {
                    id: handleItem
                    x: dial.background.x + dial.background.width / 2 - width / 2
                    y: dial.background.y + dial.background.height / 2 - height / 2
                    width: 16
                    height: 16
                    color: dial.pressed ? "#17a81a" : "#21be2b"
                    radius: 8
                    antialiasing: true
                    opacity: dial.enabled ? 1 : 0.3
                    transform: [
                        Translate {
                            y: -Math.min(dial.background.width, dial.background.height) * 0.4 + handleItem.height / 2
                        },
                        Rotation {
                            angle: dial.angle
                            origin.x: handleItem.width / 2
                            origin.y: handleItem.height / 2
                        }
                    ]
                }
        }

        TextArea {
            id: textArea
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

            Layout.row: 3
            Layout.rowSpan: 2
            Layout.column: 0
            Layout.columnSpan: 4

            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
            wrapMode: Text.Wrap
            textFormat: Text.AutoText
            hoverEnabled: true
            placeholderText: qsTr("Text Area")

            background: Rectangle {
                    implicitWidth: 200
                    implicitHeight: 40
                    border.color: textArea.enabled ? "#21be2b" : "transparent"
                }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
