import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.11
import QtMultimedia 5.15

Page {
    id: page
    property alias videoBtn: videoBtn
    property alias progressBar: progressBar
    property alias camera: camera
    property alias player: player

    header: Label {
        text: qsTr("ЛР 2. Мультимедия")
        font.pixelSize: Qt.application.font.pixelSize * 1.5
        padding: 10
    }

    MediaPlayer {
        id: player
        source: "movies/sample.mp4"
        autoPlay: false
        loops: 99
    }

    Camera {
        id: camera

        imageProcessing.whiteBalanceMode: CameraImageProcessing.ColorFilterNone

        exposure {
            exposureCompensation: -1.0
            exposureMode: Camera.ExposurePortrait
        }

        flash.mode: Camera.FlashRedEyeReduction

        imageCapture {
            onImageSaved: {
                console.log(path)
            }
        }

        videoRecorder.audioEncodingMode: CameraRecorder.ConstantBitRateEncoding
        videoRecorder.audioBitRate: 128000
        videoRecorder.mediaContainer: "mp4"
        videoRecorder.outputLocation: "captured_video"
        videoRecorder.audioChannels: 2
        videoRecorder.audioCodec: "aac"
        videoRecorder.audioSampleRate: 512
        videoRecorder.frameRate: 30
    }

    ButtonGroup {
        buttons: mediaRadioButtons.children
    }

    ColumnLayout {
        anchors.fill: parent

        RowLayout {
            id: mediaRadioButtons
            Layout.alignment: Qt.AlignHCenter

            RadioButton {
                id: mediaRadio1
                checked: true
                text: qsTr("Видео")
            }
            RadioButton {
                id: mediaRadio2
                text: qsTr("Камера")
            }

            Connections {
                target: mediaRadio2 || mediaRadio1
                onCheckedChanged: {
                    if (mediaRadio1.checked) {
                        videoPlayer.visible = true
                        cameraPlayer.visible = false
                        camera.stop()
                        player.pause()
                    } else if (mediaRadio2.checked) {
                        cameraPlayer.visible = true
                        videoPlayer.visible = false
                        camera.stop()
                        player.pause()
                        camera.start()
                    }
                }
            }
        }

        Rectangle {
            id: videoPlayer
            color: "gray"
            Layout.fillWidth: true
            Layout.fillHeight: true

            ColumnLayout {
                anchors.fill: parent

                VideoOutput {
                    id: videoOutput
                    source: player
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                }

                Slider {
                    Layout.fillWidth: true
                    id: progressBar
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                    from: 0
                    to: player.duration
                    value: player.position

                    Connections {
                        onMoved: {
                            player.seek(progressBar.value)
                        }
                    }
                }

                Button {
                    id: videoBtn
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                    text: (player.playbackState == 1) ? "Pause" : "Play"

                    Connections {
                        onReleased: {
                            if (player.playbackState == 1) {
                                player.pause()
                            } else if (player.playbackState == 2) {
                                player.play()
                            } else if (player.playbackState == 0) {
                                player.play()
                            }
                        }
                    }
                }
            }
        }

        Rectangle {
            id: cameraPlayer
            visible: false
            color: "gray"
            Layout.fillWidth: true
            Layout.fillHeight: true

            ColumnLayout {
                anchors.fill: parent

                VideoOutput {
                    id: cameraOutput
                    source: camera
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    autoOrientation: true
                }

                RowLayout {
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

                    Button {
                        id: recordBtn
                        text: "Record"
                        state: '0'

                        Connections {
                            onReleased: {
                                if (recordBtn.state === '0') {
                                    camera.videoRecorder.record()
                                    recordBtn.text = 'Stop'
                                    pauseRecordBtn.visible = true
                                    recordBtn.state = '1'
                                } else {
                                    camera.videoRecorder.stop()
                                    recordBtn.text = 'Record'
                                    pauseRecordBtn.visible = false
                                    recordBtn.state = '0'
                                }
                            }
                        }
                    }

                    Button {
                        id: pauseRecordBtn
                        visible: false
                        text: "Pause Record"
                        state: '0'

                        Connections {
                            onReleased: {
                                if (pauseRecordBtn.state === '0') {
                                    camera.videoRecorder.stop()
                                    pauseRecordBtn.text = 'Continue'
                                    pauseRecordBtn.state = '1'
                                } else {
                                    camera.videoRecorder.record()
                                    pauseRecordBtn.text = 'Pause Record'
                                    pauseRecordBtn.state = '0'
                                }
                            }
                        }
                    }

                    Button {
                        id: photoBtn
                        text: "Photo"

                        Connections {
                            onReleased: {
                                camera.imageCapture.captureToLocation(
                                            "capturedPhoto" + Date.now(
                                                ) + ".jpg") //photo/
                            }
                        }
                    }
                }
            }
        }
    }
}



