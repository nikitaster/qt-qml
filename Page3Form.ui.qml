import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.11

Page {
    header: Label {
        text: qsTr("ЛР 3. HTTP/HTTPS")
        font.pixelSize: Qt.application.font.pixelSize * 1.5
        padding: 10
    }
    ScrollView {
        //        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: parent.height

        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
        ScrollBar.vertical.policy: ScrollBar.AlwaysOff

        GridLayout {
            width: parent.width
            columns: 2
            rows: 4

            Button {
                Layout.fillWidth: true
                Layout.column: 0
                Layout.columnSpan: 1
                text: "Get Previous"

                objectName: "btnRequestBack"
                onClicked: {
                    WebAppController.goBack()
                }
            }
            Button {
                Layout.fillWidth: true
                Layout.column: 1
                Layout.columnSpan: 1
                text: "Get Next"
                objectName: "btnRequestNext"
                onClicked: {
                    WebAppController.goNext()
                }
            }

            Text {
                Layout.alignment: Qt.AlignRight | Qt.AlignTop
                Layout.column: 0
                Layout.columnSpan: 1
                Layout.row: 1

                font.pixelSize: Qt.application.font.pixelSize
                font.bold: true
                text: "UserId: "
            }
            Text {
                Layout.column: 1
                Layout.columnSpan: 1
                Layout.row: 1

                font.pixelSize: Qt.application.font.pixelSize
                objectName: "textAreaResponseUserId"
                text: ""
            }

            Text {
                Layout.alignment: Qt.AlignRight | Qt.AlignTop
                Layout.column: 0
                Layout.columnSpan: 1
                Layout.row: 2

                font.bold: true
                font.pixelSize: Qt.application.font.pixelSize
                text: "RecordId: "
            }
            Text {
                Layout.column: 1
                Layout.columnSpan: 1
                Layout.row: 2

                font.pixelSize: Qt.application.font.pixelSize
                objectName: "textAreaResponseRecordId"
                text: ""
            }

            Text {
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                Layout.column: 0
                Layout.columnSpan: 2
                Layout.row: 3

                font.pixelSize: Qt.application.font.pixelSize * 1.5
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
                objectName: "textAreaResponseTitle"
                text: ""

                Layout.fillWidth: true
                wrapMode: Text.Wrap
            }

            Text {
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                Layout.column: 0
                Layout.columnSpan: 2
                Layout.row: 4
                Layout.fillWidth: true

                font.pixelSize: Qt.application.font.pixelSize * 1.2
                horizontalAlignment: Text.AlignHCenter
                objectName: "textAreaResponseBody"
                text: ""
                wrapMode: Text.Wrap
            }
        }
    }
}



