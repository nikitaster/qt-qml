import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.11
import QtGraphicalEffects 1.12

Page {
    //    property alias dopLabGlow: dopLabGlow
    header: Label {
        text: qsTr("Лаб 4. Graphical Effects")
        font.pixelSize: Qt.application.font.pixelSize * 1.5
        padding: 10
    }
    ScrollView {
        //        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: dopLabOneGrid.height

        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
        ScrollBar.vertical.policy: ScrollBar.AlwaysOff

        GridLayout {
            id: dopLabOneGrid
            width: parent.width
            columns: 2
            rows: 4

            Image {
                id: dopLabImageGlow
                Layout.column: 0
                Layout.columnSpan: 2
                Layout.row: 0
                Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
//                Layout.preferredHeight: 200
//                Layout.preferredWidth: 200
                fillMode: Image.PreserveAspectFit

                source: "images/sample.jpg"

                layer.enabled: true
                layer.effect: Glow {
                    id: dopLabGlow
                    samples: 17
                    radius: 8
                    color: "blue"
                    transparentBorder: true
                }
            }

            Text {
                text: "Glow radius:"
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.column: 0
                Layout.columnSpan: 1
                Layout.row: 1
            }
            Slider {
                id: dopLabSliderGlow
                Layout.column: 1
                Layout.columnSpan: 1
                Layout.row: 1

                value: 0.1
                onMoved: {
                    console.log(dopLabImageGlow.layer)
                }
            }

            Text {
                text: "HueSaturation:"
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.column: 0
                Layout.columnSpan: 1
                Layout.row: 2
            }
            Button {
                Layout.column: 1
                Layout.columnSpan: 1
                Layout.row: 2
            }

            Text {
                text: "RecursiveBlur:"
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.column: 0
                Layout.columnSpan: 1
                Layout.row: 3
            }
            Button {
                Layout.column: 1
                Layout.columnSpan: 1
                Layout.row: 3
            }
        }
    }
}
