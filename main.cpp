#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QQmlContext>

#include "webappcontroller.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setOrganizationName(QStringLiteral("MobDev"));
    QCoreApplication::setOrganizationDomain(QStringLiteral("Gudkov"));
#endif

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;                                       // создание объекта движка/интерпретатора QML
    const QUrl url(QStringLiteral("qrc:/main.qml"));                    // Задание файла QML-разметки для стартовой страницы окна приложения

    // констуркция ниж задает связь между событием "objectCreated" объекта "engine"
    // и коллбеком
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app,
                     [url](QObject *obj, const QUrl &objUrl) {          // callback lamda-func
                        if (!obj && url == objUrl)                      // если ошибка
                            QCoreApplication::exit(-1);                 // завершить с кодом -1 - ошибки
                     }, Qt::QueuedConnection);
    engine.load(url);                                                   // загрузить файл стартовой страницы в движок

    QObject* mainObj = engine.rootObjects()[0];
    WebAppController webAppController(mainObj);
    engine.rootContext()->setContextProperty("WebAppController", &webAppController);

    return app.exec();                                                  // передача управления от точки входа коду самого приложения (cpp, qml).
}


/*
    Строение проекта Qt QML
    *.pro - файл настройки системы сборки qmake,
    все файлы из дерева проекта перечислены в *.pro,
    при удалении из *.pro файла файлы также удаляются из проекта.

    - внешние библиотеки (.lib), header-files, подключаются через *.pro
    - различие процесса сборки для ОС задается в *.pro файле
    - main.cpp точка входа в приложегние.
    - в случае приложения QML в main создается объект движка интерпретатора QML-разметки - Chromium

    Как и в любом С++ приложении в проекте могут быть и другие cpp и h файлы.

    Описание интерфейса приложение и простейших механик его логики содержится в файлах QML,
    которые выполняют роль фронтенда. QML - диалект JS + JSON.   
*/

/*
 * .qrc - файл ресурсов, туа помещются любые некомпилируемые данные: изображения, видео, аудио...
 *
 * отладка с++ кода производится обязательно в конигурации debug и с запущенным отладчиком (f5 вместо ctrl+R),
 * кроме того отладка с++ может проивозиться печатью qDebug() << "text"; (не только в debug и не только отладчиком):
 * #include <QDebug>
 * qDebug() << 'text' << obj;
 *
 * отладка qml производится отладочной печаться console.log() или с помощью отладчика
 *
 * раскладка по Layouts и Anchors
 * если на странице графических элементов немного, то можно выравниваться каждый элемент по границам соседнего:
 * button.bottom: label.top
 *
 * Если елементов много, то лучше использоваться Лайауты:
 * - GridLayout - и по вертикали, и по горизонтали
 * - ColumnLayout - по вертикали
 * - RowLayaout - по горизонтали
 *
 * Layout.column : номер колонки с нуля
 * Layout.colspan : диапазон занимаемых колонок
 * Layout.row : номер строки с нуля
 * Layout.fillWidth: true
 * Layout.fillHeight: true
 * Layout:alignment : Qt.AlignBottom | Qt.AlignHCenter
*/

/*
 * Проекты -> сборка -> среда -> доабвить путь до openssl в path (Qt/tools...)
*/
