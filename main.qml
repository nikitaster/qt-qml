import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

ApplicationWindow {
    width: 640
    height: 480
    visible: true
    title: qsTr("Tabs")



    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page1Form {}

        Page2Form {}

        Page3Form {}

        Page4Form {}
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: qsTr("ЛР1")
        }
        TabButton {
            text: qsTr("ЛР2")
        }
        TabButton {
            text: qsTr("ЛР3")
        }
        TabButton {
            text: qsTr("ЛР4")
        }
    }
}
