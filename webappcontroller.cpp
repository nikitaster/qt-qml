#include "webappcontroller.h"

WebAppController::WebAppController(QObject *qObj) : viewer(qObj)
{
    this->NetworkAM = new QNetworkAccessManager(this);
    this->url_str = "https://jsonplaceholder.typicode.com/posts/";
    this->curApiId = 0;
    connect(this->NetworkAM, &QNetworkAccessManager::finished, this, &WebAppController::replyFinished);
}

void WebAppController::getPageInfo()
{
    QNetworkRequest request;
    QEventLoop eventLoop;

    request.setUrl(QUrl((this->url_str + std::to_string(this->curApiId)).c_str())); //http://club-nissan.ru/forums

    connect(this->NetworkAM, &QNetworkAccessManager::finished, &eventLoop, &QEventLoop::quit);
    this->NetworkAM->get(request);

    eventLoop.exec();
}

void WebAppController::goNext()
{
    this->curApiId += 1;
    this->getPageInfo();
}

void WebAppController::goBack()
{

    if (this->curApiId > 1) {
        this->curApiId -= 1;
        this->getPageInfo();
    }
}


void WebAppController::replyFinished(QNetworkReply *reply)
{
    // Вывод в GUI
    QString str = reply->readAll();
    QJsonDocument doc = QJsonDocument::fromJson(str.toUtf8());

    viewer->findChild<QObject*>("textAreaResponseUserId")->setProperty("text", QString::number(doc["userId"].toDouble()));
    viewer->findChild<QObject*>("textAreaResponseRecordId")->setProperty("text", QString::number(doc["id"].toDouble()));
    viewer->findChild<QObject*>("textAreaResponseTitle")->setProperty("text", doc["title"]);
    viewer->findChild<QObject*>("textAreaResponseBody")->setProperty("text", doc["body"]);
}
