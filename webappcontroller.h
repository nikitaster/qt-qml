#ifndef WEBAPPCONTROLLER_H
#define WEBAPPCONTROLLER_H

#include <QObject>

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QJsonDocument>
#include <QEventLoop>

class WebAppController : public QObject
{
    Q_OBJECT
    QNetworkAccessManager * NetworkAM;
    std::string url_str;
protected:
    QObject *viewer;
public:
    unsigned curApiId;
    explicit WebAppController(QObject *qObj = nullptr);
    ~WebAppController() { delete this->NetworkAM; delete this->viewer; }


public slots:
    void getPageInfo();
    void goNext();
    void goBack();

    void replyFinished(QNetworkReply * reply);
};

#endif // WEBAPPCONTROLLER_H
